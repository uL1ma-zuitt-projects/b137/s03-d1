package b137.obrera.s03d1;

public class RepetitionStatements {

    public static void main(String[] args){
        System.out.println("Repetition Statements\n");

        // for statement
        for(int i = 0; i < 5; i++){
            System.out.println(i+1);
        }

        System.out.println();
        // Mini-activity
        int[] arrayOfNumbers = {
                100, 200, 300, 400, 500
        };

        for (int i = 0; i < arrayOfNumbers.length; i++){
            System.out.println(arrayOfNumbers[i]);
        }

        System.out.println();

        for (int arrayOfNumber:arrayOfNumbers){
            System.out.println(arrayOfNumber);
        }

        System.out.println();

        String[] warriors = {
                "Curry", "Thompson", "Green", "Wiggins", "Wiseman"
        };
        for (String warrior: warriors){
            System.out.println(warrior);
        }

        System.out.println();
        // while and do-while statements

        int x = 0;
        int y = 10;

        while (x <= 10) {
            System.out.println("Loop number: " + x);
            x++;
        }

        System.out.println();

        do {
            System.out.println("Countdown: " + y);
            y--;
        } while (y >= 0);
    }
}
